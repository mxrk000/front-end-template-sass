//Import librarys js from node modules
require('jquery/dist/jquery');
require('bootstrap/dist/js/bootstrap.bundle');

//Import librarys css from node modules
require('normalize.css/normalize.css');
require('bootstrap/dist/css/bootstrap.min.css');

//Import sass
require('./styles/main.scss');
require('./js/main.js');
