# Template SASS

Template Front End with SASS

## Getting Started

### Prerequisites

What things you need to install and how to install them

```
Node.js

```

### Installing


```
- npm install

```

### Run


```
- npm run build: Compile one time
- npm run watch: Compile when save and localhost:8080

```

### Documentation

- https://webpack.js.org/
- https://www.npmjs.com/package/webpack-build-notifier
## Authors

* **Marco Marin**
